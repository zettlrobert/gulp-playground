const gulp = require('gulp');
//Minimize Images
const imagemin = require('gulp-imagemin');
//Compress JS
const uglify = require('gulp-uglify');
//Pipeline
var pipeline = require('readable-stream').pipeline;


/*
gulp.tasks      // Define Tasks
gulp.src        // Point to files to use
gulp.dest       // Points to folder to output
gulp.watch      // Watch files and folders for changes
 */


//Logs Message
gulp.task('message', function(){
  return console.log("Gulp is running...");
  resolve();
});


gulp.task('default', function(){
  return new Promise(function(resolve, reject) {
    console.log("Gulp running with default task");
    resolve();
  });
});


//// Copy all HTML Files to dist
gulp.task('copyHtml', function(){
  return new Promise(function(resolve, reject) {
    //Chain Sources in Array
    gulp.src(['src/*.html', 'src/*.css'])
      //call dist whatever you like
    .pipe(gulp.dest('dist'));
      //All Done Resolve
      resolve();
  })
})



//Optimize Images
//-> No Images


//Minify JS
gulp.task('minify',function(){
  //New Promise
  return pipeline(
    gulp.src('src/js/*.js'),
    uglify(),
    gulp.dest('dist/js')
  );
});
